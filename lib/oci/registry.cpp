#include "oci/registry.hpp"

#include <cpr/cpr.h>

#include <iostream>
#include <nlohmann/json.hpp>
#include <string_view>

#include "core.hpp"
#include "util.hpp"

namespace oci {
namespace {

AuthError parse_unauthorized(const cpr::Response& resp) {
    std::string_view auth_header = resp.header.at("www-authenticate");
    std::string_view realm, service;

    std::tie(std::ignore, realm) =
        util::split_first(auth_header, "Bearer realm=\"");

    std::tie(realm, service) = util::split_first(realm, "\"");
    std::tie(std::ignore, service) = util::split_first(service, "service=\"");
    std::tie(service, std::ignore) = util::split_first(service, "\"");
    return {std::string(realm), std::string(service)};
}

}  // namespace

class RegistryImpl : public Registry {
public:
private:
};

core::Var<core::Ptr<Registry>, AuthError> registry(const std::string& host) {
    auto resp = cpr::Get(cpr::Url{"https://" + host + "/v2"});

    switch (resp.status_code) {
        case cpr::status::HTTP_UNAUTHORIZED:
            return parse_unauthorized(resp);
        default:
            core::panic("Status code not implemented: [", resp.status_code, ']',
                        resp.error.message);
    }
    return core::Ptr<Registry>(new RegistryImpl);
}

}  // namespace oci
