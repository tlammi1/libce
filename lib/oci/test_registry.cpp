#include <gtest/gtest.h>

#include "core.hpp"
#include "oci/registry.hpp"

TEST(RegistryTest, Create) {
    core::match(
        oci::registry("index.docker.io"),
        [](const core::Ptr<oci::Registry>& /*unused*/) {},
        [](const oci::AuthError& err) {
            FAIL() << "Could not connect to registry: " << err;
        });
}