#include "logging/stream.hpp"

#include <iostream>

#include "util.hpp"

namespace logging {
namespace {

constexpr std::array PREFIX_MAP = util::make_array<std::string_view>(
    "TRACE", "DEBUG", "INFO", "NOTE", "WARN", "ERR");

// NOLINTNEXTLINE
std::mutex g_out_mutex{};
// NOLINTNEXTLINE
std::mutex g_err_mutex{};

// NOLINTNEXTLINE
Level g_level{Level::Note};

std::unique_lock<std::mutex> make_lock(std::mutex& m, bool enable) {
    return enable ? std::unique_lock{m} : std::unique_lock<std::mutex>{};
}

Stream make_stream(std::mutex* mutex, std::ostream* out, Level level) {
    if (level >= g_level) {
        Stream s{mutex, out, true};
        if (level != Level::Always)
            s << '['
              << PREFIX_MAP.at(
                     static_cast<std::underlying_type_t<Level>>(level))
              << "] ";
        return s;
    }
    return {mutex, out, false};
}

}  // namespace

void init(Level min_level) { g_level = min_level; }

Stream::Stream(std::mutex* mutex, std::ostream* out, bool enable)
    : m_lk{make_lock(*mutex, enable)}, m_out{out}, m_enable{enable} {}

Stream::Stream(Stream&& other)
    : m_lk{std::move(other.m_lk)},
      m_out{other.m_out},
      m_enable{other.m_enable} {
    other.m_enable = false;
}

Stream& Stream::operator=(Stream&& other) {
    m_lk = std::move(other.m_lk);
    m_out = other.m_out;
    m_enable = other.m_enable;
    other.m_enable = false;
    return *this;
}

Stream::~Stream() {
    if (m_enable) (*m_out) << '\n';
}

Stream out_stream(Level level) {
    return make_stream(&g_out_mutex, &std::cout, level);
}

Stream err_stream(Level level) {
    return make_stream(&g_err_mutex, &std::cerr, level);
}

}  // namespace logging