#pragma once

#include <array>
#include <string_view>
#include <tuple>
#include <vector>

namespace util {

/**
 * Split string from the first occurence of token
 *
 * \param str String to split
 * \param token Token to search for and split
 * \return string splitted from first occurence of \a token. Token is not
 * included in the string.
 *
 */
constexpr std::pair<std::string_view, std::string_view> split_first(
    std::string_view str, std::string_view token) {
    auto match = str.find(token);
    if (match == str.npos) return {str, ""};
    return {str.substr(0, match), str.substr(match + token.size())};
}

/**
 * Split string from all occurences of the given token
 *
 * \param str String to split
 * \param token Token where to split the string
 * \return Splitted string where tokens are not present
 *
 */
inline std::vector<std::string_view> split(std::string_view str,
                                           std::string_view token) {
    std::vector<std::string_view> out{};
    while (true) {
        auto idx = str.find(token);
        if (idx == str.npos) {
            out.push_back(str);
            return out;
        }
        out.push_back(str.substr(0, idx));
        str.remove_prefix(idx + token.size());
    }
}

/**
 * Helper for automatically deducing size
 *
 */
template <class T, class... Ts>
constexpr std::array<T, sizeof...(Ts)> make_array(Ts&&... ts) {
    // NOLINTNEXTLINE
    return {std::forward<Ts>(ts)...};
}

}  // namespace util