#pragma once

#include "logging/stream.hpp"

// NOLINTNEXTLINE
#define LOG_OUT ::logging::out_stream(::logging::Level::Always)
// NOLINTNEXTLINE
#define LOG_TRACE ::logging::err_stream(::logging::Level::Trace)
// NOLINTNEXTLINE
#define LOG_DEBUG ::logging::err_stream(::logging::Level::Debug)
// NOLINTNEXTLINE
#define LOG_INFO ::logging::err_stream(::logging::Level::Info)
// NOLINTNEXTLINE
#define LOG_NOTE ::logging::err_stream(::logging::Level::Note)
// NOLINTNEXTLINE
#define LOG_WARN ::logging::err_stream(::logging::Level::Warn)
// NOLINTNEXTLINE
#define LOG_ERR ::logging::err_stream(::logging::Level::Err)
