#pragma once

#include <iostream>
#include <string>

#include "core.hpp"
namespace oci {

struct AuthError : core::Struct<AuthError> {
    template <class T, class U>
    AuthError(T&& t, U&& u)
        : bearer_realm{std::forward<T>(t)}, service{std::forward<U>(u)} {}

    std::string bearer_realm;
    std::string service;
};

class Registry {
public:
    virtual ~Registry() {}

private:
};

/**
 * Create a registry connection to the given host
 *
 * The host is e.g. index.docker.io
 */
core::Var<core::Ptr<Registry>, AuthError> registry(const std::string& host);

}  // namespace oci

CORE_STRUCT(oci::AuthError, bearer_realm, service);
