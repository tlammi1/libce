#pragma once

namespace logging {

enum class Level {
    Trace,
    Debug,
    Info,
    Note,
    Warn,
    Err,
    Always,
};

constexpr auto underlying_cast(Level l) {
    return static_cast<std::underlying_type_t<Level>>(l);
}

// NOLINTNEXTLINE
#define DEFINE_OPERATOR(oper)                              \
    constexpr bool operator oper(Level l, Level r) {       \
        return underlying_cast(l) oper underlying_cast(r); \
    }

DEFINE_OPERATOR(>)
DEFINE_OPERATOR(>=)
DEFINE_OPERATOR(<)
DEFINE_OPERATOR(<=)

#undef DEFINE_OPERATOR

}  // namespace logging
