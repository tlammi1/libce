#pragma once

#include <mutex>
#include <ostream>
#include <string_view>

#include "logging/level.hpp"

namespace logging {

class Stream {
public:
    Stream(std::mutex* mutex, std::ostream* out, bool enable = true);

    ~Stream();

    Stream(const Stream&) = delete;
    Stream& operator=(const Stream&) = delete;
    Stream(Stream&& other);
    Stream& operator=(Stream&& other);

    template <class T>
    Stream& operator<<(T&& t) {
        if (m_enable) (*m_out) << std::forward<T>(t);
        return *this;
    }

private:
    std::unique_lock<std::mutex> m_lk;
    std::ostream* m_out;
    bool m_enable;
};

void init(Level min_level);
Stream out_stream(Level level);
Stream err_stream(Level level);

}  // namespace logging
