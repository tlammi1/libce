#pragma once

#include <variant>

namespace core {
namespace detail {

template <class... Ts>
struct Overload : public Ts... {
    using Ts::operator()...;
};

template <class... Ts>
Overload(Ts...) -> Overload<Ts...>;

}  // namespace detail

template <class... Ts>
using Var = std::variant<Ts...>;

template <class V>
struct is_variant : std::false_type {};

template <class... Ts>
struct is_variant<Var<Ts...>> : std::true_type {};

template <class V>
constexpr bool is_variant_v = is_variant<V>::value;

template <class Variant, class... Ts>
constexpr auto match(Variant&& v, Ts&&... ts) {
    static_assert(is_variant_v<std::decay_t<Variant>>,
                  "Match called for non-variant");
    return std::visit(detail::Overload{std::forward<Ts>(ts)...},
                      std::forward<Variant>(v));
}

}  // namespace core