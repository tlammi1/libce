#pragma once

#include <sstream>
#include <string>

namespace core {
namespace detail {
[[noreturn]] void panic(std::string&& str);

template <class T, class... Ts>
void accumulate_stream(std::stringstream& s, T&& t, Ts&&... ts) {
    s << std::forward<T>(t);
    if constexpr (sizeof...(Ts)) {
        accumulate_stream(s, std::forward<Ts>(ts)...);
    }
}
}  // namespace detail

template <class... Ts>
[[noreturn]] void panic(Ts&&... ts) {
    std::stringstream ss{};
    detail::accumulate_stream(ss, std::forward<Ts>(ts)...);
    detail::panic(ss.str());
}
}  // namespace core