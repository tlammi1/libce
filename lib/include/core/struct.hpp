#pragma once

#include <string_view>

#include "thirdparty/visit_struct.hpp"

namespace core {

template <class T>
struct Struct {
    static constexpr std::string_view name() noexcept {
        return visit_struct::get_name<T>();
    }

    constexpr bool operator==(const Struct& other) const noexcept {
        bool match = true;
        visit_struct::for_each(
            *this, other,
            [&](const char* /*unused*/, const auto& l, const auto& r) {
                if (l != r) match = false;
            });
        return match;
    }

    constexpr bool operator!=(const Struct& other) const noexcept {
        return !(*this == other);
    }
};

template <class T>
std::ostream& operator<<(std::ostream& ss, const Struct<T>& st) {
    ss << Struct<T>::name() << "{\n";
    const T& ref = static_cast<const T&>(st);
    visit_struct::for_each(ref, [&](const char* name, const auto& value) {
        ss << name << ": " << value << '\n';
    });
    ss << '}';
    return ss;
}

// NOLINTNEXTLINE
#define CORE_STRUCT VISITABLE_STRUCT

}  // namespace core