#pragma once

#include <memory>

#include "core/panic.hpp"
#include "core/struct.hpp"
#include "core/var.hpp"

namespace core {

template <class T>
using Ptr = std::unique_ptr<T>;

}
