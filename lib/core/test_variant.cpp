#include <gtest/gtest.h>

#include "core/var.hpp"

TEST(Match, Simple) {
    core::Var<int, const char*> v{100};
    bool res = core::match(
        v, [](int) -> bool { return true; },
        [](const char*) -> bool { return false; });

    ASSERT_TRUE(res);
    v = "asdf";
    res = core::match(
        v, [](int) -> bool { return true; }, [](const char*) { return false; });
    ASSERT_FALSE(res);
}