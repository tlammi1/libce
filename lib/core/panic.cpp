#include "core/panic.hpp"

#include <exception>

namespace core::detail {

[[noreturn]] void panic(std::string&& str) {
#if defined(__cpp_exceptions)
    throw std::runtime_error(std::move(str));
#else
    std::terminate();
#endif
}

}  // namespace core::detail