#!/bin/sh

set -e

THISDIR="$(dirname $0)"
cd $THISDIR

build_gcc(){
	if [ ! -d build-gcc ]; then
		meson -Dallow_fallbacks=true build-gcc
	fi
}

build_clang(){
	if [ ! -d build-clang ]; then
		CC=clang CXX=clang++ meson -Dallow_fallbacks=true build-clang
	fi
}


test_gcc(){
	(
	build_gcc
	cd build-gcc
	meson test
	)
}

test_clang(){
	(
	build_clang
	cd build-clang
	meson test
	)
}


compile_gcc(){
	(
	build_gcc
	cd build-gcc
	meson compile
	)
}

compile_clang(){
	(
	build_clang
	cd build-clang
	meson compile
	)
}

_test(){
	test_gcc
	test_clang
}

compile(){
	compile_gcc
	compile_clang
}

compile
_test

